const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const restrict = require("../helpers/restrict")
const authorize = require("../helpers/authorize")
const roles = require('../utils/roles')

//user
router.get('/userBio', restrict, controller.biodata.show);

//admin
router.get('/allBio', authorize(roles.admin), controller.biodata.allBiodata);
router.get('/specificBio/:userId', authorize(roles.admin), controller.biodata.specificBiodata);
router.post('/createBio/:userId', authorize(roles.admin), controller.biodata.create);
router.put('/updateBio/:userId', authorize(roles.admin), controller.biodata.update);
router.delete('/deleteBio/:userId', authorize(roles.admin), controller.biodata.delete);

module.exports = router;