const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const storage = require('../utils/media/storage')


router.post('/image/single', storage.image.single('image'), controller.media.imageSingle);
router.post('/image/multiple', storage.image.array('image'), controller.media.imageMultiple);
router.post('/video/single', storage.video.single('video'), controller.media.videoSingle);
router.post('/video/multiple', storage.video.array('video'), controller.media.videoMultiple);

module.exports = router
