const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const mid = require("../helpers/middleware");
const restrict = require("../helpers/restrict")
const authorize = require("../helpers/authorize")
const roles = require('../utils/roles');
const m = require('../helpers/middleware')



//User Endpoint

router.get('/payment', authorize(roles.user), controller.auth.payment)
router.get('/auth/resetPassword', controller.auth.resetPassView)
router.post('/auth/resetPassword', controller.auth.resetPass)
router.get('/auth/forgotPassword', controller.auth.forgotPassView)
router.post('/auth/forgotPassword', controller.auth.forgotPassword)
router.get('/auth/signup', controller.auth.signUp)
router.post('/auth/signup', controller.auth.register);
router.get('/auth/signin', controller.auth.signIn)
router.post('/auth/signin', controller.auth.login);
router.get('/auth/whoami', restrict, controller.auth.whoami)


//Admin Endpoint
router.delete('/auth/deleteuser/:userId', authorize(roles.admin), controller.auth.deleteDataUser);
router.put('/auth/changePassword/:userId', authorize(roles.admin), controller.auth.changePassword);
router.get('/auth/allUser', authorize(roles.admin), controller.auth.getAllUser)

module.exports = router;