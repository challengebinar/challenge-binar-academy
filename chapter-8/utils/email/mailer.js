require('dotenv').config()
const nodemailer = require('nodemailer')
const { google } = require('googleapis')
const ejs = require('ejs')
// const express = require('express')
// const app = express()

// app.use(express.json())
// app.use

const {
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    GOOGLE_REDIRECT_URI,
    GOOGLE_SENDER_EMAIL,
    GOOGLE_REFRESH_TOKEN,
} = process.env;

const oAuth2Client = new google.auth.OAuth2(
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    GOOGLE_REDIRECT_URI
)

oAuth2Client.setCredentials({ refresh_token: GOOGLE_REFRESH_TOKEN });
module.exports = {
    // getHtml: async (filename, data) => {
    //     const path = __dirname + '/../../views/email/' + filename
    // },

    sendMail: async (to, subject, html) => {
        return new Promise(async (resolve, reject) => {
            try {
                // get access token from google
                const accessToken = await oAuth2Client.getAccessToken();
                const transport = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        type: "OAuth2",
                        user: GOOGLE_SENDER_EMAIL,
                        clientId: GOOGLE_CLIENT_ID,
                        clientSecret: GOOGLE_CLIENT_SECRET,
                        refreshToken: GOOGLE_REFRESH_TOKEN,
                        accessToken: accessToken
                    }
                });

                const mailOptions = {
                    to,
                    subject,
                    html
                };

                const response = await transport.sendMail(mailOptions);

                resolve(response);
            } catch (err) {
                reject(err);
            }
        });
    },

    // render email template
    getHtml: (filename, data) => {
        return new Promise(async (resolve, reject) => {
            try {
                const path = __dirname + '/../../views/' + filename;
                ejs.renderFile(path, data, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    },

    // kirimEmail: async (userName, userEmail) => {
    //     try {
    //         const html = await getHtml('welcome-email.ejs', { user: { name: userName } })
    //         const response = await this.sendMail(userEmail, 'Selamat datang Gamers', html)
    //     } catch (err) {
    //         console.log(err)
    //     }
    // }
}

