require('dotenv').config()
const { VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY, VAPID_SUBJECT } = process.env
const express = require('express');
const morgan = require('morgan');
//const passport = require('./utils/passport)
const router = require('./routes');
const cors = require('cors')
const webpush = require('web-push')
const path = require('path');
// const methodOverride = require('method-override');

const app = express();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'client')));
app.use(morgan('dev'));
app.set('view engine', 'ejs');
app.use(router);
app.use("/images", express.static("public/images"));
app.use("/videos", express.static("public/videos"));
// app.use(methodOverride('_method'));


// app.use(passport.initialize())

webpush.setVapidDetails(VAPID_SUBJECT, VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

app.post('/notification/subscribe', async (req, res) => {
    try {
        const subscription = req.body
        const payload = JSON.stringify({
            title: 'Selamat datang',
            body: 'Selamat datang gamers'
        })
        webpush.sendNotification(subscription, payload)
            .then(result => console.log(result))
            .catch(e => console.log(e.stack))

        return res.status(200).json({
            status: true,
            message: ''
        })
    } catch (err) {
        console.log(err)
        return res.status(500).json({
            status: false,
            message: err.message
        })
    }
})

// 404 handler
app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'Are you lost?'
    });
});

// 500 handler
app.use((err, req, res, next) => {
    console.log(err);
    return res.status(500).json({
        status: false,
        message: err.message
    });
});
// const testingHelpers = require('./helpers/testingHelpers');
// testingHelpers.userTruncate();
// testingHelpers.biodataTruncate();
// testingHelpers.historyTruncate();

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => { console.log('listening on port', PORT) })

