'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class otp extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  otp.init({
    user_id: DataTypes.INTEGER,
    otp: DataTypes.STRING,
    expiration_time: {
      type: DataTypes.DATE,
      set(value) {
        const date = new Date()
        this.setDataValue('expiration_time', new Date(date.getTime() + value * 60000))
      }
    }
  }, {
    sequelize,
    modelName: 'otp',
  });
  return otp;
};