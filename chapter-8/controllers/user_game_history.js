const { user_game, user_game_history } = require('../models');
const jwt = require('jsonwebtoken');

const {
  JWT_SIGNATURE_KEY
} = process.env;



module.exports = {
  create: async (req, res, next) => {
    try {
      const { userId } = req.params;
      const { score } = req.body;

      const existUser = await user_game.findOne({ where: { id: userId } })
      if (!existUser) {
        return res.status(404).json({
          status: false,
          message: 'user not found'
        });
      }

      const Createhistory = await user_game_history.create({
        user_id: userId,
        score
      });
      return res.status(201).json({
        status: true,
        message: 'success',
        data: Createhistory
      });
    } catch (err) {
      next(err);
    }
  },

  show: async (req, res, next) => {
    try {
      const userHistory = await user_game_history.findAll({ where: { user_id: req.user.id } })
      if (userHistory == "") {
        return res.status(404).json({
          status: false,
          message: 'user history not found',
        });
      }
      return res.status(200).json({
        status: true,
        message: 'success',
        data: userHistory
      });
    } catch (err) {
      next(err);
    }
  },

  allHistory: async (req, res, next) => {
    try {
      const userHistory = await user_game_history.findAll()
      if (userHistory == "") {
        return res.status(404).json({
          status: false,
          message: 'user history not found',
        });
      }
      return res.status(200).json({
        status: true,
        message: 'success',
        data: userHistory
      });
    } catch (err) {
      next(err);
    }
  },

  specificHistory: async (req, res, next) => {
    try {
      const { userId } = req.params
      const userHistory = await user_game_history.findAll({ where: { user_id: userId } })
      if (userHistory == "") {
        return res.status(404).json({
          status: false,
          message: 'user history not found',
        });
      }
      return res.status(200).json({
        status: true,
        message: 'success',
        data: userHistory
      });
    } catch (err) {
      next(err);
    }
  },

  update: async (req, res, next) => {
    try {
      const { userId } = req.params;
      const { historyId } = req.params;
      const { score } = req.body;

      const userHistory = await user_game_history.findOne({ where: { user_id: userId, id: historyId } })
      if (!userHistory) {
        return res.status(404).json({
          status: false,
          message: 'history not found!',

        });
      }
      if (score) {
        const historyScore = await user_game_history.update(
          { score, }, { where: { user_id: userId, id: historyId }, }
        );
      }
      const hasil = await user_game_history.findOne({ where: { user_id: userId, id: historyId } })
      return res.status(200).json({
        status: true,
        message: "Success updated biodata",
        data: hasil
      });
    } catch (err) {
      next(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      const { userId } = req.params;
      const { historyId } = req.params;
      const userHistory = await user_game_history.findOne({ where: { user_id: userId, id: historyId } })
      if (!userHistory) {
        return res.status(404).json({
          status: false,
          message: 'History not found!',
        });
      }
      const userGameHistory = await user_game_history.destroy({
        where: { user_id: userId, id: historyId },
      });
      return res.status(200).json({
        status: true,
        message: "history has been deleted",
      });
    }
    catch (err) {
      next(err);
    }
  }
}


