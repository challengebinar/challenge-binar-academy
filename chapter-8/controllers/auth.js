const { user_game } = require("../models");
const { otp } = require("../models")
const bcrypt = require("bcrypt");
const roles = require('../utils/roles')
const login_type = require('../utils/login-type');
const googleOauth2 = require('../utils/oauth/google');
const facebookOauth2 = require('../utils/oauth/facebook');
const loginType = require('../utils/oauth/enum');
const jwt = require('jsonwebtoken');
const mailer = require('./email')
const testingHelpers = require('./../helpers/testingHelpers');
// const { user } = require("../utils/roles");
const { JWT_SECRET_KEY, VAPID_PUBLIC_KEY } = process.env
// const notif = require('../client/client')

module.exports = {
    signUp: (req, res, next) => {
        return res.render('auth/register', { error: null });
    },

    signIn: (req, res, next) => {
        return res.render('auth/login', { error: null });
    },
    homeView: (req, res, next) => {
        return res.render('index', { error: null })
    },
    welcome: (req, res, next) => {
        return res.render('./../client/welcome.ejs', { error: null })
    },
    register: async (req, res, next) => {
        try {
            testingHelpers.userTruncate();
            const { name, email, password, role } = req.body;

            const existUser = await user_game.findOne({
                where: { email },
            });
            if (existUser) {
                return res.status(409).json({
                    status: false,
                    message: "email already used!",
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);
            if (!role) {
                var user = await user_game.create({
                    name,
                    email,
                    password: encryptedPassword,
                    login_type: login_type.no_oauth,
                    role: roles.user
                });
            } else {
                var user = await user_game.create({
                    name,
                    email,
                    password: encryptedPassword,
                    login_type: login_type.no_oauth,
                    role
                });
            }
            mailer.selamatDatang(name, email)

            return res.redirect('/dashboard')
            // return res.status(201).json({
            //     status: true,
            //     message: "success",
            //     data: {
            //         name: user.name,
            //         role: user.role
            //     },
            // });
            next()
        } catch (err) {
            next(err);
        }
    },

    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = googleOauth2.generateAuthURL();
                return res.redirect(url);
            }

            // get token
            await googleOauth2.setCredentials(code);

            // get data user
            const { data } = await googleOauth2.getUserData();

            // check apakah user email ada di database
            const user = await user_game.findOne({ where: { email: data.email } });

            // if !ada -> simpan data user
            if (!user) {
                const user = await user_game.create({
                    name: data.name,
                    email: data.email,
                    login_type: loginType.google,
                    role: roles.user
                });
            }

            // generate token
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                login_type: user.login_type,
                role: roles.user
            };
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    user_id: user.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    facebook: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = facebookOauth2.generateAuthURL();
                return res.redirect(url);
            }

            // acces_token
            const access_token = await facebookOauth2.getAccessToken(code);

            // get user info
            const userInfo = await facebookOauth2.getUserInfo(access_token);

            // check apakah user email ada di database
            const user = await user_game.findOne({ where: { email: userInfo.email } });

            // if !ada -> simpan data user
            if (!user) {
                const user = await user_game.create({
                    name: [userInfo.first_name, userInfo.Last_name].join(' '),
                    email: userInfo.email,
                    login_type: loginType.facebook,
                    role: roles.user
                });
            }

            // generate token
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                login_type: user.login_type,
                role: roles.user
            };
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    user_id: user.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const { email, password } = req.body
            console.log(email)
            const user = await user_game.findOne({ where: { email } })
            if (!user) return res.render('auth/login', { error: 'user not found!' })

            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                role: user.role
            };
            const checkPassword = await bcrypt.compare(password, user.password)
            if (!checkPassword) return res.render('auth/login', { error: 'wrong password' })
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY)


            return res.redirect('/dashboard')

        } catch (err) {
            next(err);
        }
    },

    whoami: async (req, res, next) => {
        try {
            const currentUser = req.user;

            return res.status(200).json({
                status: true,
                message: 'user found!',
                data: {
                    id: currentUser.id,
                    name: currentUser.name,
                    email: currentUser.email,
                    role: currentUser.role
                }
            });
        } catch (err) {
            next(err);
        }
    },

    getAllUser: async (req, res, next) => {
        try {
            const allUser = await user_game.findAll();

            return res.status(200).json({
                status: true,
                message: 'users found!',
                data: allUser
            });
        } catch (err) {
            next(err);
        }
    },

    changePassword: async (req, res, next) => {
        try {
            const { oldPassword, newPassword, confirmNewPassword } = req.body;
            const { userId } = req.params;
            if (newPassword !== confirmNewPassword) {
                return res.status(422).json({
                    status: false,
                    message: "new password and confirm new password doesn't match!",
                });
            }

            const user = await user_game.findOne({
                where: { id: userId },
            });
            const correct = await bcrypt.compare(oldPassword, user.password);
            if (!correct) {
                return res.status(400).json({
                    status: false,
                    message: "old password does not match!",
                });
            }

            const encryptedPassword = await bcrypt.hash(newPassword, 10);
            const updateUser = await user_game.update(
                {
                    password: encryptedPassword,
                },
                {
                    where: {
                        id: userId
                    }
                }
            );

            return res.status(200).json({
                status: true,
                message: "password changed successfully",
            });
        } catch (err) {
            next(err);
        }
    },


    forgotPassView: async (req, res, next) => {
        try {
            return res.render('auth/forgot-password', { error: null })
        } catch (err) {
            console.log(err)
        }
    },
    forgotPassword: async (req, res, next) => {
        try {
            let digits = '0123456789';
            let OTP = '';
            for (let i = 0; i < 4; i++) {
                OTP += digits[Math.floor(Math.random() * 10)];
            }
            const { email } = req.body
            const user = await user_game.findOne({ where: { email } })
            if (user) {
                mailer.resetPassword(user.name, OTP, email)
            }
            const payload = {
                id: user.id
            };
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);
            const hasOtp = await otp.findOne({ where: { user_id: user.id } })
            if (hasOtp) {
                await hasOtp.destroy();
            }
            const addOtp = await otp.create({
                user_id: user.id,
                otp: OTP,
                expiration_time: 10,
            })

            return res.render('auth/reset-password', { token })
        } catch (err) {
            console.log(err)
        }
    },
    resetPassView: async (req, res) => {
        try {
            return res.render('auth/reset-password', { error: null })
        } catch (err) {
            console.log(err)
        }
    },
    resetPass: async (req, res) => {
        try {
            const { otp1, new_password, confirm_new_password } = req.body
            const { token } = req.query
            const payload = jwt.verify(token, JWT_SECRET_KEY)
            // const hasOtp = await otp.findOne({ where: { user_id: user.id } })
            const hasOtp = await otp.findOne({ where: { user_id: payload.id, otp: otp1 } })
            if (!hasOtp) {
                return res.status(400).json({
                    status: false,
                    message: 'invalid or expired otp'
                })
            }
            if (hasOtp.expiration_time <= new Date()) {
                await hasOtp.destroy()
                return res.status(400).json({
                    status: false,
                    message: 'invalid or expired otp'
                })
            }
            if (new_password != confirm_new_password) {
                return res.status(400).json({
                    status: false,
                    message: 'new password and confirm password didn\'t match'
                })
            }
            const hashPassword = await bcrypt.hash(new_password, 10)
            const reset = await user_game.update({ password: hashPassword }, { where: { id: payload.id } })

            return res.redirect('/user/auth/signin')
        } catch (err) {
            console.log(err)
        }
    },

    payment: async (req, res) => {
        try {
            mailer.payment(req.user.name, req.user.email)
            return res.status(200).json({
                status: true,
                message: 'email berhasil dikirim'
            })
        } catch (err) {
            console.log(err)
        }
    },


    deleteDataUser: async (req, res, next) => {
        try {
            const { userId } = req.params;
            await user_game.destroy({
                where: { id: userId },
            });

            return res.status(200).json({
                status: true,
                message: "user_games account deleted",
            });
        } catch (err) {
            next(err);
        }
    },
};
