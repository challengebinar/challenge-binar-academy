const mailer = require('../utils/email/mailer');
module.exports = {
    selamatDatang: async (userName, userEmail) => {

        try {
            const html = await mailer.getHtml("email/welcome-email.ejs", { user: { name: userName } });
            const sendEmail = await mailer.sendMail(userEmail, "Selamat datang Gamers", html);
            return sendEmail
        } catch (err) {
            console.log(err);
        }
    },

    resetPassword: async (name, otp, email) => {
        try {
            const html = await mailer.getHtml("email/reset-password.ejs", { user: { name, otp } });
            const sendEmail = await mailer.sendMail(email, "Reset Password", html);
            return sendEmail
        } catch (err) {
            console.log(err)
        }
    },
    payment: async (name, email) => {
        try {
            const html = await mailer.getHtml("email/payment.ejs", { user: { name } });
            const sendEmail = await mailer.sendMail(email, "Konfirmasi Pembayaran", html);
            return sendEmail
        } catch (err) {
            console.log(err)
        }
    }
}

