const { user_game, user_game_biodata } = require('../models');
const jwt = require('jsonwebtoken');

const {
  JWT_SIGNATURE_KEY
} = process.env;

module.exports = {
  create: async (req, res, next) => {
    try {
      const { userId } = req.params
      const { name, gender, region, phone } = req.body;

      const existUser = await user_game.findOne({ where: { id: userId } })
      if (!existUser) {
        return res.status(404).json({
          status: false,
          message: 'user not found'
        });
      }


      const existBio = await user_game_biodata.findOne({ where: { id_user: userId } });
      if (existBio) {
        return res.status(409).json({
          status: false,
          message: 'biodata already created!'
        });
      }
      const Createuser = await user_game_biodata.create({
        id_user: userId,
        name,
        gender,
        region,
        phone
      });

      return res.status(201).json({
        status: true,
        message: 'success',
        data: 'successfully create user biodata'
      });
    } catch (err) {
      next(err);
    }
  },

  show: async (req, res, next) => {
    try {
      const userBiodata = await user_game_biodata.findOne({ where: { id_user: req.user.id } })
      if (!userBiodata) {
        return res.status(404).json({
          status: false,
          message: 'user bio not found',
          data: null
        });
      }
      return res.status(200).json({
        status: true,
        message: 'success',
        data: userBiodata
      });
    } catch (err) {
      next(err);
    }
  },

  allBiodata: async (req, res, next) => {
    try {
      const userBiodata = await user_game_biodata.findAll()
      if (!userBiodata) {
        return res.status(404).json({
          status: false,
          message: 'user bio not found',
          data: null
        });
      }
      return res.status(200).json({
        status: true,
        message: 'success',
        data: userBiodata
      });
    } catch (err) {
      next(err);
    }
  },

  specificBiodata: async (req, res, next) => {
    try {
      const { userId } = req.params
      const userBiodata = await user_game_biodata.findOne({ where: { id_user: userId } })
      if (!userBiodata) {
        return res.status(404).json({
          status: false,
          message: 'user bio not found',
          data: null
        });
      }
      return res.status(200).json({
        status: true,
        message: 'success',
        data: userBiodata
      });
    } catch (err) {
      next(err);
    }

  },

  update: async (req, res, next) => {
    try {
      const { name, gender, region, phone } = req.body;
      const { userId } = req.params;

      const userBiodata = await user_game_biodata.findOne({ where: { id_user: userId } })
      if (!userBiodata) {
        return res.status(404).json({
          status: false,
          message: 'biodata not found!',
          data: null
        });
      }
      if (name) {
        const userName = await user_game_biodata.update(
          { name, }, { where: { id_user: userId }, }
        );
      }

      if (gender) {
        const userGender = await user_game_biodata.update(
          { gender, }, { where: { id_user: userId }, }
        );
      }

      if (region) {
        const userRegion = await user_game_biodata.update(
          { region, }, { where: { id_user: userId }, }
        );
      }

      if (phone) {
        const userPhone = await user_game_biodata.update(
          { phone, }, { where: { id_user: userId }, }
        );
      }
      //const hasil = await user_game_biodata.findOne({ where: { id_user: id_user } })
      return res.status(200).json({
        status: true,
        message: "Success updated biodata",

      });
    } catch (err) {
      next(err);
    }
  },

  delete: async (req, res, next) => {
    try {
      const { userId } = req.params;
      const userBiodata = await user_game_biodata.findOne({ where: { id_user: userId } })
      if (!userBiodata) {
        return res.status(404).json({
          status: false,
          message: 'biodata not found!',
          data: null
        });
      }
      const userGameBiodata = await user_game_biodata.destroy({
        where: { id_user: userId },
      });
      return res.status(200).json({
        status: true,
        message: "bio has been deleted",
      });
    }
    catch (err) {
      next(err);
    }
  }
}


