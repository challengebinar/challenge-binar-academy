const pi = 3.14;
let pilihan, nilai1, nilai2;

const tambah = (a,b) => {return  +a + +b};

const kurang = (a,b) => {return +a - +b};

const kali = (a,b) => {return +a * +b};

const bagi = (a,b) => {return +a/+b};

const akar = (angka) => {return Math.sqrt(+angka)};

const kuadrat = (a,b) => {return Math.pow(a,b)};

const luasPersegi = (sisi) => {return +sisi * +sisi};

const volumeKubus = (sisi) => {return sisi**3};

const volumeTabung = (r,t) => {return pi*(r**2)*+t};

const readLine = require('readline');

const interface = readLine.createInterface({
    input : process.stdin,
    output : process.stdout
})

function input(question){
    return new Promise(resolve => {
        interface.question(question, data =>{
            return resolve(data);
        })
    })
}  

async function main(){
    try{
        console.log("Program Kalkulasi\n\n");
        console.log("Silahkan pilih salah satu\n");
        console.log("1. Tambah");
        console.log("2. Kurang");
        console.log("3. Kali");
        console.log("4. Bagi");
        console.log("5. Akar");
        console.log("6. Kuadrat");
        console.log("7. Luas Persegi");
        console.log("8. Volume Kubus");
        console.log("9. Volume Tabung");
        let pil = await input("\nMasukkan pilihan(1-9) : ");
        pilihan = pil;
        if(pilihan == 1 || pilihan == 2 || pilihan == 3 || pilihan == 4){
            let n1 = await input("\nMasukkan nilai 1 : ");
            nilai1 = n1;
            let n2 = await input("Masukkan nilai 2 : ");
            nilai2 = n2;
            if(pilihan == 1){console.log(`Hasil : ${tambah(nilai1, nilai2)}`)}
            else if(pilihan == 2){console.log(`Hasil : ${kurang(nilai1, nilai2)}`)}
            else if(pilihan == 3){console.log(`Hasil : ${kali(nilai1, nilai2)}`)}
            else if(pilihan == 4){console.log(`Hasil : ${bagi(nilai1, nilai2)}`)}
            
        } 
        else if(pilihan == 5){
            let n1 = await input("\nMasukkan nilai : ");
            nilai1 = n1;
            console.log(`Hasil : ${akar(nilai1)}`);}
        else if(pilihan == 6){
            let n1 = await input("\nMasukkan nilai : ");
            nilai1 = n1;
            let n2 = await input("Masukkan nilai : ");
            nilai2 = n2;
            console.log(`Hasil : ${kuadrat(nilai1,nilai2)}`);
      
        } 
        else if(pilihan == 7 || pilihan == 8){
            let n1 = await input("\nMasukkan sisi : ");
            nilai1 = n1;
            if(pilihan == 7) console.log(`Hasil : ${luasPersegi(nilai1)}`);
            else if(pilihan == 8) console.log(`Hasil : ${volumeKubus(nilai1)}`); 
           
        } 
        else if(pilihan == 9){
            let n1 = await input("\nMasukkan jari-jari alas tabung : ");
            nilai1 = n1;
            let n2 = await input("Masukkan tinggi tabung : ");
            nilai2 = n2;
            console.log(`Hasil : ${volumeTabung(nilai1, nilai2)}`);
            
        }
        let ulang = await input("\nApakah ingin mengulang?(y/n) : ");
        pilihan = ulang;
        if(pilihan == "y") main();
        else interface.close();
        
    }
    catch(err){
        console.log(err)
        main();
    } 
}



main();

