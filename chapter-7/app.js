require('dotenv').config()
const express = require('express');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
//const passport = require('./utils/passport)
const router = require('./routes');
const app = express();

app.use(express.json());
app.use(morgan('dev'));
app.use(router);
app.use("/images", express.static("public/images"));
app.use("/videos", express.static("public/videos"));

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(passport.initialize())



// 404 handler
app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'Are you lost?'
    });
});

// 500 handler
app.use((err, req, res, next) => {
    console.log(err);
    return res.status(500).json({
        status: false,
        message: err.message
    });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => { console.log('listening on port', PORT) })

