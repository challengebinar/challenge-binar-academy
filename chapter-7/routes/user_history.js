const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const restrict = require("../helpers/restrict")
const authorize = require("../helpers/authorize")
const roles = require('../utils/roles')

//user
router.get('/showHistory', restrict, controller.history.show);

//admin
router.get('/allHistory', authorize(roles.admin), controller.history.allHistory);
router.get('/spesificHistory/:userId', authorize(roles.admin), controller.history.specificHistory);
router.post('/createHistory/:userId', authorize(roles.admin), controller.history.create);
router.delete('/deleteHistory/:userId/:historyId', authorize(roles.admin), controller.history.delete);
router.put('/updateHistory/:userId/:historyId', authorize(roles.admin), controller.history.update);

module.exports = router;