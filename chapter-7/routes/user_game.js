const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const mid = require("../helpers/middleware");
const restrict = require("../helpers/restrict")
const authorize = require("../helpers/authorize")
const roles = require('../utils/roles');


//User Endpoint


router.post('/auth/register', controller.auth.register);
router.post('/auth/login', controller.auth.login);
router.get('/auth/whoami', restrict, controller.auth.whoami)


//Admin Endpoint
router.delete('/auth/deleteuser/:userId', authorize(roles.admin), controller.auth.deleteDataUser);
router.put('/auth/changePassword/:userId', authorize(roles.admin), controller.auth.changePassword);
router.get('/auth/allUser', authorize(roles.admin), controller.auth.getAllUser)

module.exports = router;