const express = require("express");
const user_game = require("./user_game");
const user_bio = require("./user_bio");
const user_history = require("./user_history");
const media = require("./media")
const router = express.Router();
const controller = require("../controllers");

router.use("/user", user_game);
router.use("/bio", user_bio);
router.use("/upload", media);
router.use("/history", user_history);


router.get('/api/auth/login', controller.auth.google);
router.get('/api/auth/login/facebook', controller.auth.facebook);


module.exports = router;