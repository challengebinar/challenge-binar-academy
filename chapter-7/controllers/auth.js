const { user_game } = require("../models");
const bcrypt = require("bcrypt");
const roles = require('../utils/roles')
const login_type = require('../utils/login-type');
const googleOauth2 = require('../utils/oauth/google');
const facebookOauth2 = require('../utils/oauth/facebook');
const loginType = require('../utils/oauth/enum');
const jwt = require('jsonwebtoken');

module.exports = {
    register: async (req, res, next) => {
        try {
            const { name, email, password, role } = req.body;

            const existUser = await user_game.findOne({
                where: { email },
            });
            if (existUser) {
                return res.status(409).json({
                    status: false,
                    message: "email already used!",
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);
            if (req.body.role == "") {
                var user = await user_game.create({
                    name,
                    email,
                    password: encryptedPassword,
                    login_type: login_type.no_oauth,
                    role: roles.user
                });
            } else {
                var user = await user_game.create({
                    name,
                    email,
                    password: encryptedPassword,
                    login_type: login_type.no_oauth,
                    role
                });
            }


            return res.status(201).json({
                status: true,
                message: "success",
                data: {
                    name: user.name,
                    role: user.role
                },
            });
        } catch (err) {
            next(err);
        }
    },

    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = googleOauth2.generateAuthURL();
                return res.redirect(url);
            }

            // get token
            await googleOauth2.setCredentials(code);

            // get data user
            const { data } = await googleOauth2.getUserData();

            // check apakah user email ada di database
            const user = await user_game.findOne({ where: { email: data.email } });

            // if !ada -> simpan data user
            if (!user) {
                const user = await user_game.create({
                    name: data.name,
                    email: data.email,
                    login_type: loginType.google,
                    role: roles.user
                });
            }

            // generate token
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                login_type: user.login_type,
                role: roles.user
            };
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    user_id: user.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    facebook: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = facebookOauth2.generateAuthURL();
                return res.redirect(url);
            }

            // acces_token
            const access_token = await facebookOauth2.getAccessToken(code);

            // get user info
            const userInfo = await facebookOauth2.getUserInfo(access_token);

            // check apakah user email ada di database
            const user = await user_game.findOne({ where: { email: userInfo.email } });

            // if !ada -> simpan data user
            if (!user) {
                const user = await user_game.create({
                    name: [userInfo.first_name, userInfo.Last_name].join(' '),
                    email: userInfo.email,
                    login_type: loginType.facebook,
                    role: roles.user
                });
            }

            // generate token
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                login_type: user.login_type,
                role: roles.user
            };
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    user_id: user.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const user = await user_game.authenticate(req.body);
            const accesstoken = user.generateToken();

            return res.status(200).json({
                status: true,
                message: "success",
                data: {
                    name: user.name,
                    role: user.role,
                    token: accesstoken
                },
            });
        } catch (err) {
            next(err);
        }
    },

    whoami: async (req, res, next) => {
        try {
            const currentUser = req.user;

            return res.status(200).json({
                status: true,
                message: 'user found!',
                data: {
                    id: currentUser.id,
                    name: currentUser.name,
                    email: currentUser.email,
                    role: currentUser.role
                }
            });
        } catch (err) {
            next(err);
        }
    },

    getAllUser: async (req, res, next) => {
        try {
            const allUser = await user_game.findAll();

            return res.status(200).json({
                status: true,
                message: 'users found!',
                data: allUser
            });
        } catch (err) {
            next(err);
        }
    },

    changePassword: async (req, res, next) => {
        try {
            const { oldPassword, newPassword, confirmNewPassword } = req.body;
            const { userId } = req.params;
            if (newPassword !== confirmNewPassword) {
                return res.status(422).json({
                    status: false,
                    message: "new password and confirm new password doesn't match!",
                });
            }

            const user = await user_game.findOne({
                where: { id: userId },
            });
            const correct = await bcrypt.compare(oldPassword, user.password);
            if (!correct) {
                return res.status(400).json({
                    status: false,
                    message: "old password does not match!",
                });
            }

            const encryptedPassword = await bcrypt.hash(newPassword, 10);
            const updateUser = await user_game.update(
                {
                    password: encryptedPassword,
                },
                {
                    where: {
                        id: userId
                    }
                }
            );

            return res.status(200).json({
                status: true,
                message: "password changed successfully",
            });
        } catch (err) {
            next(err);
        }
    },

    deleteDataUser: async (req, res, next) => {
        try {
            const { userId } = req.params;
            await user_game.destroy({
                where: { id: userId },
            });

            return res.status(200).json({
                status: true,
                message: "user_games account deleted",
            });
        } catch (err) {
            next(err);
        }
    },
};
