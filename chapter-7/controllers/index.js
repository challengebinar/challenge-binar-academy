const auth = require('./auth')
const biodata = require('./user_game_biodata')
const history = require('./user_game_history')
const media = require('./media')

module.exports = { auth, biodata, history, media }