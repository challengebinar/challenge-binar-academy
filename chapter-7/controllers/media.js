const storage = require("../utils/media/storage")

module.exports = {
    imageSingle: (req, res) => {
        const imageUrl = req.protocol + "://" + req.get("host") + "/images/" + req.file.filename;

        return res.json({
            imageUrl
        });
    },

    imageMultiple: (req, res) => {

        const files = [];
        req.files.forEach(file => {
            const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + file.filename;

            files.push(imageUrl);
        });

        return res.json(files);
    },

    videoSingle: (req, res) => {
        const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + req.file.filename;

        return res.json({
            videoUrl
        });
    },

    videoMultiple: (req, res) => {

        const files = [];
        req.files.forEach(file => {
            const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + file.filename;

            files.push(videoUrl);
        });

        return res.json(files);
    }

}

