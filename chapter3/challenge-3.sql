-- Challenge - 3 Binar Academy 
-- Daniel Manurung 

-- CREATE DATABASE perpustakaan

--DDL

-- membuat tabel anggota
CREATE TABLE anggota(
    id_anggota BIGSERIAL PRIMARY KEY,
    nama VARCHAR(50) NOT NULL,
    jenis_kelamin VARCHAR(50) NOT NULL,
    no_hp VARCHAR(50) NOT NULL,
    alamat VARCHAR(50) NOT NULL
);

-- membuat tabel buku
CREATE TABLE buku(
    id_buku BIGSERIAL PRIMARY KEY,
    judul VARCHAR(50) NOT NULL,
    kategori VARCHAR(50) NOT NULL,
    pengarang VARCHAR(50) NOT NULL
);

--membuat tabel petugas
CREATE TABLE petugas(
    id_petugas BIGSERIAL PRIMARY KEY,
    password VARCHAR(50) NOT NULL,
    nama VARCHAR(50) NOT NULL,
    no_hp VARCHAR(50) NOT NULL
);

--membuat tabel peminjaman
CREATE TABLE peminjaman(
    id_peminjaman BIGSERIAL PRIMARY KEY,
    id_anggota INT NOT NULL,
    id_buku int NOT NULL,
    tgl_pinjam DATE NOT NULL,
    tgl_kembali DATE NOT NULL,
    id_petugas int NOT NULL
);

--membuat tabel pengembalian
CREATE TABLE pengembalian(
    id_pengembalian BIGSERIAL PRIMARY KEY,
    tgl_pengembalian DATE NOT NULL,
    denda INT NOT NULL,
    id_buku INT NOT NULL,
    id_anggota INT NOT NULL,
    id_petugas INT NOT NULL
);

--Operasi CRUD dengan DML

--CREATE data kedalam masing-masing tabel

--Mengisi data kedalam tabel anggota
INSERT INTO anggota(nama, jenis_kelamin, no_hp, alamat) VALUES
('Daniel', 'laki-laki', '082345354657', 'Medan'),
('Manurung', 'laki-laki', '089787655674', 'Medan'),
('Izaz', 'laki-laki', '087676787678', 'Medan');

--Mengisi data kedalam table buku
INSERT INTO buku(judul, kategori, pengarang) VALUES
('Facta Dokumenta Jakarta ( Niaga )', 'Sejarah Indonesia', 'Ridwan Saidi'),
('Undang-Undang Informasi & Transaksi Elektronik', 'Undang-undang', 'Tim Legality'),
('Java Untuk Kriptografi + cd', 'Pengenalan Komputer', 'Abdul Kadir');

--Mengisi data kedalam tabel petugas
INSERT INTO petugas(password, nama, no_hp) VALUES
('admin', 'Jeremi', '083456377654'),
('admin2', 'Syifaul', '098765467892'),
('admin3', 'Umar', '083425674839');

--Mengisi data kedalam tabel peminjaman
INSERT INTO peminjaman(id_anggota, id_buku, tgl_pinjam, tgl_kembali, id_petugas) VALUES
(1, 1, '2022-09-01', '2022-09-08', 1),
(2, 2, '2022-09-01', '2022-09-08', 2),
(3, 3, '2022-09-01', '2022-09-08', 3);

--Mengisi data kedalam tabel pengembalian
INSERT INTO pengembalian(tgl_pengembalian, denda, id_buku, id_anggota, id_petugas) VALUES
('2022-09-08', 0, 1, 1, 1),
('2022-09-08', 0, 2, 2, 2),
('2022-09-08', 0, 3, 3, 3);


--Read isi dari masing-masing tabel
SELECT * FROM anggota;

--Read tabel buku dengan id_buku = 1
SELECT * FROM buku WHERE id_buku = 1; 
SELECT * FROM petugas;
SELECT * FROM peminjaman;
SELECT * FROM pengembalian;

--Update salah satu data dari tabel anggota 
UPDATE anggota SET
    nama = 'Rico',
    jenis_kelamin = 'laki-laki',
    no_hp = '086754738282',
    alamat = 'Bali'
WHERE id_anggota = 1;

--Delete salah satu data dari tabel peminjaman
DELETE FROM peminjaman WHERE id_peminjaman = 3;


