//Challenge Chapter 5
//Daniel Manurung

const {
    Collection,
    Item,
    Header
} = require("postman-collection");
const fs = require("fs");

// create collection
const postmanCollection = new Collection({
    info: {
        name: "Challenge-4 API Documentation",
    },
    item: [],
});

/*set headers
const rawHeaderString =
    'Authorization:\nContent-Type:application/json\ncache-control:no-cache\n';

const rawHeader = Header.parse(rawHeaderString);

const requestHeader = rawHeader.map((h) => new Header(h));*/
//User game

//register user game
const authRegister = new Item({
    name: "Register User Game",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },

        url: "http://localhost:3000/user/auth/register",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                username: "daniel10",
                password: "password123",
            }),
        },
        auth: null,
    },
});

//login user game
const authLogin = new Item({
    name: "Login user",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/user/auth/login",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                username: "daniel10",
                password: "password123",
            }),
        },
        auth: null,
    },
});

//dhange password user
const authChangePassword = new Item({
    name: "Change password user",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/user/auth/changepassword",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                oldPassword: "password321",
                newPassword: "password123",
                confirmNewPassword: "password123",
            }),
        },
        auth: null,
    },
});

//delete user
const authDeleteUser = new Item({
    name: "Delete user",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/user/auth/deleteuser",
        method: "DELETE",
        auth: null,
    },
});

//user game biodata

//Get detail user game biodata
const bioGetDetail = new Item({
    name: "detail user game biodata",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/bio/userBio/",
        method: "GET",
        auth: null,
    },
});

//Create user game biodata
const bioCreateBiodata = new Item({
    name: "Create user game biodata",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/bio/createBio",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                name: "dimas",
                gender: "male",
                region: "indonesia",
                phone: "082334354556",
            }),
        },
        auth: null,
    },
});

//update biodata
const bioUpdateOneOfBiodata = new Item({
    name: "Update one of user biodata's data ",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/bio/updateBio/",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                name: "dimas G",
            }),
        },
        auth: null,
    },
});

//update bio
const bioUpdateBiodata = new Item({
    name: "Update Biodata",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/bio/updateBio/",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                name: "jasmine",
                gender: "female",
                region: "egypt",
                phone: "083293847584",
            }),
        },
        auth: null,
    },
});

//delete user game bidata
const bioDeleteBio = new Item({
    name: "Delete user game biodata",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/bio/deleteBio/",
        method: "DELETE",
        auth: null,
    },
});

//user game history

//create user game history
const hisCreatehistory = new Item({
    name: "Create new user game history",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/history/createHistory",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                date: "2022-09-27",
                time: "16:15:25",
                score: 50,
            }),
        },
        auth: null,
    },
});

//show
const hisShowHistory = new Item({
    name: "Show game history of user ",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/history/showHistory/",
        method: "GET",
        auth: null,
    },
});

//update history
const hisUpdateHistory = new Item({
    name: "Update User History",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/history/updateHistory/:historyId",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                date: "2022-09-25",
                time: "14:14:26",
                score: 70,
            }),
        },
        auth: null,
    },
});

//delete history
const hisDeleteHistory = new Item({
    name: "Delete User History",
    request: {
        header: {
            "Content-type": "application/json",
            Authorization: "",
            "cache-control": "no-cache",
        },
        url: "http://localhost:3000/deleteHistory/:historyId",
        method: "DELETE",
        auth: null,
    },
});

//user game
postmanCollection.items.add(authLogin);
postmanCollection.items.add(authRegister);
postmanCollection.items.add(authChangePassword);
postmanCollection.items.add(authDeleteUser);

//biodata
postmanCollection.items.add(bioCreateBiodata);
postmanCollection.items.add(bioGetDetail);
postmanCollection.items.add(bioUpdateBiodata);
postmanCollection.items.add(bioUpdateOneOfBiodata);
postmanCollection.items.add(bioDeleteBio);

//history
postmanCollection.items.add(hisCreatehistory);
postmanCollection.items.add(hisShowHistory);
postmanCollection.items.add(hisUpdateHistory);
postmanCollection.items.add(hisDeleteHistory);

// convert to json
const collectionJSON = postmanCollection.toJSON();

//export to file
fs.writeFile("./collection.json", JSON.stringify(collectionJSON), (err) => {
    if (err) console.log(err);
    console.log("file saved");
});