let grade = [];

const interface = require('readline').createInterface({
    input : process.stdin,
    output : process.stdout
})

function input(question){
    return new Promise(resolve => {
        interface.question(question, data =>{
            return resolve(data);
        })
    })
} 

const MaxMin = () => {
    console.log("Nilai Tertinggi : " + Math.max(...grade));
    console.log("Nilai Terendah : " + Math.min(...grade));
  };

const kelulusan = () => {
    let jumlahLulus = 0;
    let tidakLulus = 0;
    grade.forEach(function (data) {
      if (data >= 60) {
        jumlahLulus++;
      }else{
        tidakLulus++;
      }
      
    });
    console.log(`Jumlah siswa yang lulus : ${jumlahLulus}`);
    console.log(`jumalah siswa tidak lulus : ${tidakLulus}`);
}

function main(){
    console.log(`Input nilai dan tekan "d" jika sudah selesai`);
    inputNilai();  
}

async function inputNilai(){
    try{
        let nilai = await input(``);
        
        if (nilai == "d"){
            interface.close();
            Hasil();
           
        }
        else if(isNaN(nilai) || nilai<0 || nilai>100){
            console.log("Silahkan masukkan nilai 1-100");
            inputNilai();
        }
       else{
            grade.push(+nilai);
            inputNilai();
        } 
       
    }
    catch(err){
        console.log(err);
    }
}

function urutan(){
    grade.sort(function(a,b){return a-b})
    return grade; 
}

const rataRataNilai = () => {
    let jum = 0;
    grade.forEach(function (data) {
        jum += data;
    });
    let average = jum / grade.length;
    return average.toFixed(2);
    };

function filterNilai(){
    const nilai90 = grade.filter((element) => element === 90);
    const nilai100 = grade.filter((element) => element === 100);
    return `${nilai90},${nilai100}`
}

function Hasil(){
    MaxMin();
    console.log("Rata - rata nilai : "+rataRataNilai());
    kelulusan();
    console.log("Urutan nilai siswa : "+urutan());
    console.log("Siswa dengan nilai 90 dan 100 : "+filterNilai());
}

main();
